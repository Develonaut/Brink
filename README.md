# Brink
You can view a production build of the landing page here (Note: all assets and code are minified to help with performance): ​https://develonaut.github.io/Brink/

# Project initiative

I decided to design and develop an application that would provide notifications on the topic of Hockey. I enjoy Hockey and feel like it’s still tough to find good coverage of the news inside the league. I felt there was an opportunity to provide a service to users who want to be more engaged with the happenings of Hockey.

# Designing the landing page

The ultimate goal of the landing page is to entice the user into signing up and downloading the application. To achieve this, I designed something clean and simple. Using white space makes it easy for the user to quickly identify the call to actions and the critical information. As a result, the page utilized flat designs with subtle imagery and engaging animations.

The landing page mockups range from mobile to desktop layouts. The design was completed in a mobile-first approach which helped streamline the elements that were important to display on the landing page. There are three call to action buttons the user can press since the idea of the application is to allow the user to get notifications on any device. The user can sign up, and would be taken to a service page to download the app. I thought having a button for desktop and mobile would be appealing to the user, as well, since some users are apprehensive about signing up for services with no immediate return. Showing the user they can download a desktop or mobile application without going through a sign up flow coerces them into feeling instant gratification for their effort.

# Designing the display ads

The approach for the display ads was to imagine them in their application environments and create something that would contrast against its host page, but still keep true to the feeling of the brand design in styling related to the landing pages. Designing in this way trains the user from our ingress points so that they know what to expect from our app before they start using it. I also wanted to be sure that the ads would stand out on a dark or light theme.
 
# Developing the landing page

The landing page relies heavily on HTML and CSS to achieve it’s looks and feel. One of the biggest aspects is the use of CSS grid which is very powerful and makes mocking up layouts and converting them into responsive web pages across multiple devices painless. It’s important to note the social links at the bottom currently don’t point anywhere but I included them since the idea was that Brink as a service would have social media accounts to use.

The JavaScript for this page is relatively minimal. The JS is mainly there to add some very basic form validation for the Sign Up-flow, provide some smooth scrolling to different areas of the page and provides classes to slide elements in so the user is engaged as they scroll. For the form validation, it’s currently only preventing the user from submitting an empty form. I would imagine with specification into the submit parameters we could add other validation to verify the data in each field matches what we expect. Note, on submit of the Sign Up Form you can view the submitted payload data in the developer tools.

Overall this project was a lot of fun, and I had a great time brainstorming the application from start to finish. Developing the branding was a great exercise and paired great with problem-solving regarding conveying information about the product while balancing the "fun" of the brand. These experiences are where my passions lie and are what make me feel incredibly excited about pairing Web Design and Development.

