$( document ).ready(function() {
  $(document).on("click", "[data-type=jump_to]", smoothScroll);
  $(document).on("click", "[data-type=submit]", validateForm);
  $(document).on("keyup", "input", validateInput);

  $(window).scroll(function(event) {
    $(".module").each(function(i, el) {
      var el = $(el);
      if (el.visible(true)) {
        el.addClass("come-in"); 
      } 
    });
  });
});

// Turn serialized form data into an object
// so we can do validation checks.
function JSONify(search) {
  return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
}

// Helper function to help jump around the landing page.
function smoothScroll(event) {
  event.preventDefault();

  const button = $(event.currentTarget),
        target = $(`.${button.data("target")}`);

  $('html, body').animate({
    scrollTop: target.offset().top
  }, 500, function() {
    if (target.hasClass("signup-form")) {
      const firstInput = target.find("input").first();
      firstInput.focus();
    }
  });
}

// Remove the error if theres a value since the only validation
// right now is checking if the input isn't empty.
function validateInput(event) {
  const input = $(event.currentTarget),
        defPlaceholder = input.data("placeholder"),
        value = input.val();

  if (input.hasClass("error") && value.length) {
    input.removeClass("error");
    input.attr("placeholder", defPlaceholder);
  }
}

function validateForm(event) {
  event.preventDefault();

  const button = $(event.currentTarget),
        form = button.parents("form"),
        payload = JSONify(form.serialize());

  let validInputs = 0;
  Object.keys(payload).map(name => {
    const value = payload[name],
          input = $(`#${name}`);
    
    // Check the value of the input if no values
    // exists, add the error class.
    if (!value.length) {
      input.addClass("error");
      input.attr("placeholder", "Field cannot be empty");
      return false;
    }

    // If we get here the input has a value, add it to the count.
    validInputs = validInputs + 1;
  });

  // If total number of valid inputs equals the amount of fields
  // submitting with our payload then all fields have been cleared.
  // NOTE: This is fragile and more to give the feel of validation for the faux
  // landing page.
  if (Object.keys(payload).length === validInputs) {
    submitForm(payload);
    resetForm.call(form);
  }
}

// Resets the form on success so the user can
// put in new creds.
function resetForm() {
  $(this).find("input").map((idx, el) => {
    $(el).val("");
    if (idx === 0) {
      $(el).focus();
    }
  });
}

// Would put the Ajax call to a service here
function submitForm(payload) {
  console.log(`Submit the Form payload:`);
  console.log(payload);
  // Something like this:
  // $.ajax({
  //   url:"/",
  //   type: "POST",
  //   data: payload,
  //   success: function (res) {
  //     console.log(payload);
  //   },
  //   error: function (res) {
  //     throw res;
  //   },
  //   complete: function () {
  //     console.log('call is all done');
  //   }
  // });
}